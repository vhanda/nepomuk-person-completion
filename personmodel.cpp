/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2011  Vishesh Handa <handa.vish@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "personmodel.h"
#include <qdiriterator.h>

#include <QDebug>

PersonModel::PersonModel(QObject* parent): QAbstractListModel(parent)
{
    QDirIterator iter( QLatin1String("/home/vishesh/avatars"), QDir::Files );
    while( iter.hasNext() ) {
        iter.next();
        m_list << iter.fileInfo();
    }
}

int PersonModel::rowCount(const QModelIndex& parent) const
{
    return m_list.size();
}

QModelIndex PersonModel::index(int row, int column, const QModelIndex& parent) const
{
    return QAbstractListModel::index(row, column, parent);
}

QVariant PersonModel::data(const QModelIndex& index, int role) const
{
    if( !index.isValid() )
        return QVariant();

    if( index.row() < 0 || index.row() >= m_list.size() )
        return QVariant();

    QFileInfo fileInfo = m_list.at( index.row() );
    switch( role ) {
        case PictureRole: {
            QPixmap pic;
            pic.load( fileInfo.absoluteFilePath() );

            return QVariant::fromValue( pic );
        }
        case Qt::DisplayRole :
            return fileInfo.fileName();
    }

    return QVariant();
}
