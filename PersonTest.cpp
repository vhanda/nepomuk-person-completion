#include "PersonTest.h"
#include "personmodel.h"
#include "persondelegate.h"

#include <QtGui/QLabel>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QAction>

#include <QtGui>

PersonTest::PersonTest()
{
    QLineEdit * lineEdit = new QLineEdit( this );

    QListView* view = new QListView( this );
    view->setItemDelegateForColumn( 0, new PersonDelegate( this ) );

    setCentralWidget( lineEdit );

    QCompleter * comp = new QCompleter( new PersonModel, this );
    comp->setCompletionRole( Qt::DisplayRole );
    comp->setPopup( view );

    lineEdit->setCompleter( comp );
}

PersonTest::~PersonTest()
{}

#include "PersonTest.moc"
