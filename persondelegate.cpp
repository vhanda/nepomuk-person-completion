/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2011  Vishesh Handa <handa.vish@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "persondelegate.h"
#include "personmodel.h"

#include <QtGui>

#include <QDebug>

PersonDelegate::PersonDelegate(QObject* parent): QStyledItemDelegate(parent)
{

}

void PersonDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QPixmap pic = index.model()->data( index, PersonModel::PictureRole ).value<QPixmap>();
    QString name = index.model()->data( index, Qt::DisplayRole ).toString();

    QRect rect = option.rect;

    if( option.state & QStyle::State_Selected )
        painter->fillRect( rect, option.palette.highlight() );

    if( option.state & QStyle::State_MouseOver )
        painter->fillRect( rect, option.palette.dark() ); //FIXME: Proper brush.

    // Random background so that I can see the boundaries
//     QColor color( qrand() % 256, qrand() % 256, qrand() %256 );
//     painter->fillRect( rect, color );

    QSize decorationSize = option.decorationSize;
    QPoint pos = rect.topLeft();
    pos.setX( pos.x() + decorationSize.width()/2 );
    pos.setY( pos.y() + decorationSize.height()/2 );

    painter->drawPixmap( pos.x(), pos.y(), 32, 32, pic );

    pos += QPoint( 32 + decorationSize.width()/2 , 32 );

    painter->drawText( pos.x(), pos.y() - option.fontMetrics.height(), name );
    //painter->drawText( rect.topLeft().x()+20, rect.topLeft().y(), QLatin1String("fire!") );
    //QStyledItemDelegate::paint(painter, option, index);
}

QSize PersonDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    const QAbstractItemModel *model = index.model();
    QPixmap pic = model->data( index, PersonModel::PictureRole ).value<QPixmap>();
    QString name = model->data( index, Qt::DisplayRole ).toString();

//     int w = 0;
//     w += 32;
//     w += option.fontMetrics.width( name );

    QSize size = option.decorationSize;
    size.setWidth( size.width() + 32 );
    size.setHeight( size.height() + 32 );

    //qDebug() << size;
    return size;
    //return pic.size() + option.decorationSize;
    //return QStyledItemDelegate::sizeHint(option, index);
}
